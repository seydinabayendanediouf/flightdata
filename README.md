# Flight Data

Flight data provides Flights between two cities according to a time period and including the price.

# Api
Read flights
----
This REST API allows retrieving flights using filters.

[source]
----
GET /v1/flights
----

Allowed parameters for filtering

| RequestParam      | Description | Required     |  Example|
|   ---        |    ---  |          --- |            ---|
| airportDestination | Airport Destination - Code IATA|NO| DSS |
| airportOrigin | Airport Origin - Code IATA|NO|DLA|
| cityOrigin       | City Origin       |    NO    | Thies|
| cityDestination        | City Destination   |    NO    | Tunis
| fromDepartureDate          | Filter from start date of departure     |    NO    | 2020-12-01T10:00:00|
| toDepartureDate          | Filter to  end date of departure     |    NO    | 2020-12-01T10:00:00|
| fromArrivalDate          | Filter from start date of arrival     |    NO    | 2020-12-01T10:00:00|
| toArrivalDate          | Filter to  end date of arrival     |    NO    | 2020-12-01T10:00:00|

Response

* IMPORTANT : The response have to be pageable

[source, json]
```json
{
  "content": [
    {
      "flightId": 6,
      "price": 256,
      "currency": "EUR",
      "airline": "Air Côte d'Ivoire",
      "origin": {
        "airportName": "Tunis-Carthage International Airport",
        "airportCity": "Tunis"
      },
      "destination": {
        "airportName": "Blaise Diagne International Airport",
        "airportCity": "Thies"
      },
      "departureDate": "2020-12-05T10:00:00.000+00:00",
      "arrivalDate": "2020-12-06T10:00:00.000+00:00"
    }
  ],
  "pageable": {
    "sort": {
      "sorted": false,
      "unsorted": true,
      "empty": true
    },
    "offset": 0,
    "pageNumber": 0,
    "pageSize": 20,
    "paged": true,
    "unpaged": false
  },
  "totalElements": 1,
  "totalPages": 1,
  "last": true,
  "size": 20,
  "number": 0,
  "sort": {
    "sorted": false,
    "unsorted": true,
    "empty": true
  },
  "numberOfElements": 1,
  "first": true,
  "empty": false
}
```

### Deployment

App deployed using gitlab-ci and Kubernetes.

FlightData Service

[![image](https://www.linkpicture.com/q/Pod-running.png)](https://www.linkpicture.com/view.php?img=LPic5fc53c653f5031928116511)

Postgres Service

[![image](https://www.linkpicture.com/q/postgrespod.png)](https://www.linkpicture.com/view.php?img=LPic5fc544249ebfb1520727579)

Flight Data App is available here:
```sh
http://35.224.164.209:8080/swagger-ui.html#/
```

#### Run it locally
In the project directory:
```sh
$ ./mvnw spring-boot:run
```
Swagger will be available 
```sh
localhost:8080/swagger-ui.html
```