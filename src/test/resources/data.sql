INSERT INTO airline (airline_iata, airline_name, airline_country) VALUES
	('HC', 'Air Senegal', 'Senegal'),
	('TU', 'TunisAir', 'Tunisia');

INSERT INTO airport (airport_iata, airport_name, airport_city, airport_country) VALUES
	('DSS', 'Blaise Diagne International Airport', 'Thies', 'Senegal'),
	('DLA', 'Douala International Airport', 'Douala', 'Cameroon');

INSERT INTO flight (flight_id, flight_price, airline, airport_origin,airport_destination, departure_date, arrival_date) VALUES
	(1, 300, 'HC' , 'DSS', 'DLA','2020-12-01T10:00:00','2020-12-01T12:00:00'),
	(2, 200, 'TU' , 'DLA', 'DSS','2020-12-02T10:00:00','2020-12-02T13:00:00');
