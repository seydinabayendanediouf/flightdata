package com.bizao.flightdata.service.services;

import javax.transaction.Transactional;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.junit.runner.RunWith;
import com.bizao.flightdata.service.services.dto.FlightDTO;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@ActiveProfiles(profiles = {"test"})
@Transactional
public class FlightServiceTest {
    @Autowired
    private FlightService flightService;

    @Test
    public final void testReadFlights() throws Exception {

        /* Getting subscriptions */
        Page<FlightDTO> flights = flightService.readFlightByFilter(null, null, null, null, null, null, null, null,  PageRequest.of(1, 1));

        Assert.assertNotNull(flights);
        Assert.assertEquals("Wrong size of flights", 2, flights.getTotalElements());
    }
}
