package com.bizao.flightdata.service.services;

import java.util.Date;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.bizao.flightdata.service.services.dto.FlightDTO;

/**
 * Service retrieving Flight by filters
 *
 * @author Seydina
 */
public interface FlightService {
    Page<FlightDTO> readFlightByFilter(String airportOrigin, String airportDestination, String originCity, String destinationCity,
                                       Date fromDepartureDate, Date toDepartureDate, Date fromArrivalDate, Date toArrivalDate, Pageable pageable);

    }
