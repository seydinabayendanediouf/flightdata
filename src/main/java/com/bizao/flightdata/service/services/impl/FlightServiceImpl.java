package com.bizao.flightdata.service.services.impl;

import java.util.Date;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.bizao.flightdata.service.mappers.FlightMapper;
import com.bizao.flightdata.service.repositories.FlightRepository;
import com.bizao.flightdata.service.services.FlightService;
import com.bizao.flightdata.service.services.dto.FlightDTO;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@Transactional(readOnly = false)
@FieldDefaults(level = AccessLevel.PRIVATE)
@RequiredArgsConstructor
public class FlightServiceImpl implements FlightService {

    final FlightRepository flightRepository;
    final FlightMapper flightMapper;

    @Override
    public Page<FlightDTO> readFlightByFilter(String airportOrigin, String airportDestination, String originCity, String destinationCity, Date fromDepartureDate, Date toDepartureDate, Date fromArrivalDate, Date toArrivalDate, Pageable pageable) {

    return flightRepository.findAllByFilters(airportOrigin,airportDestination, originCity,  destinationCity, fromDepartureDate,  toDepartureDate,  fromArrivalDate, toArrivalDate, pageable).map(flightMapper::asFlightDTO);

    }
}
