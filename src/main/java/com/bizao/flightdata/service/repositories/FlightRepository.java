package com.bizao.flightdata.service.repositories;

import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import com.bizao.flightdata.service.entities.Flight;
import com.bizao.flightdata.service.repositories.custom.FlightRepositoryCustom;

@Repository
public interface FlightRepository extends CrudRepository<Flight, Long>, FlightRepositoryCustom, QuerydslPredicateExecutor<Flight> {
}
