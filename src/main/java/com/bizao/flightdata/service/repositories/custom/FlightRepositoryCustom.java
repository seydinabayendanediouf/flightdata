package com.bizao.flightdata.service.repositories.custom;

import java.util.Date;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.query.Param;
import com.bizao.flightdata.service.entities.Flight;
/**
 * Custom repository for filtering using query DSL
 *
 * @author Seydina
 */
public interface FlightRepositoryCustom {
    Page<Flight> findAllByFilters(
            @Param("airportOrigin") String airportOrigin,
            @Param("airportDestination") String airportDestination,
            @Param("originCity") String originCity,
            @Param("destinationCity") String destinationCity,
            @Param("fromDepartureDate") Date fromDepartureDate,
            @Param("toDepartureDate") Date toDepartureDate,
            @Param("fromArrivalDate") Date fromArrivalDate,
            @Param("toArrivalDate") Date toArrivalDate,
            Pageable pageable);
}
