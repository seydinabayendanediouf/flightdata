package com.bizao.flightdata.service.repositories.custom.impl;

import java.util.Date;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.bizao.flightdata.service.entities.Flight;
import com.bizao.flightdata.service.entities.QFlight;
import com.bizao.flightdata.service.repositories.FlightRepository;
import com.bizao.flightdata.service.repositories.custom.FlightRepositoryCustom;
import com.querydsl.core.BooleanBuilder;

public class FlightRepositoryCustomImpl implements FlightRepositoryCustom {
    @Autowired
    private FlightRepository flightRepository;

    @Override
    public Page<Flight> findAllByFilters(String airportOrigin,
                                         String airportDestination,
                                         String originCity,
                                         String destinationCity,
                                         Date fromDepartureDate,
                                         Date toDepartureDate,
                                         Date fromArrivalDate,
                                         Date toArrivalDate,
                                         Pageable pageable) {
        BooleanBuilder builder = new BooleanBuilder();

        if (StringUtils.isNotEmpty(airportOrigin)) {
            builder.and(QFlight.flight.airportOrigin.airportIata.eq(airportOrigin));
        }
        if (StringUtils.isNotEmpty(airportDestination)) {
            builder.and(QFlight.flight.airportDestination.airportIata.eq(airportDestination));
        }

        if (StringUtils.isNotEmpty(originCity)) {
            builder.and(QFlight.flight.airportOrigin.airportCity.eq(originCity));
        }
        if (StringUtils.isNotEmpty(destinationCity)) {
            builder.and(QFlight.flight.airportDestination.airportCity.eq(destinationCity));
        }

        if (fromDepartureDate != null || toDepartureDate != null) {
            builder.and(QFlight.flight.departureDate.between(fromDepartureDate, toDepartureDate));
        }

        if (fromArrivalDate != null || toArrivalDate != null) {
            builder.and(QFlight.flight.departureDate.between(fromArrivalDate, toArrivalDate));
        }
        return flightRepository.findAll(builder, pageable);
    }
}
