package com.bizao.flightdata.service.mappers.flight;

import org.mapstruct.Mapper;

import org.mapstruct.ReportingPolicy;
import com.bizao.flightdata.service.entities.flight.Airport;
import com.bizao.flightdata.service.services.dto.flight.AirportDTO;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = "spring")
public interface AirportMapper {

    AirportDTO asAirportDTO(Airport airport);

}
