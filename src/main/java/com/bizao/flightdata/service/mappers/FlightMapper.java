package com.bizao.flightdata.service.mappers;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.ReportingPolicy;
import com.bizao.flightdata.service.mappers.flight.AirportMapper;
import com.bizao.flightdata.service.entities.Flight;
import com.bizao.flightdata.service.services.dto.FlightDTO;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = "spring", uses = {AirportMapper.class})
public interface FlightMapper {
    @Mappings({
            @Mapping(source = "airline.airlineName", target = "airline"),
            @Mapping(source = "airportOrigin", target = "origin"),
            @Mapping(source = "airportDestination", target = "destination")
    })
    FlightDTO asFlightDTO(Flight flight);

}
