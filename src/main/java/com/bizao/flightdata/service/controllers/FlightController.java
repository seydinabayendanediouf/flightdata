package com.bizao.flightdata.service.controllers;

import java.util.Date;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import com.bizao.flightdata.service.services.FlightService;
import com.bizao.flightdata.service.services.dto.FlightDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;

/**
 * REST API of flight management
 *
 * @author Seydina
 */
@RestController
@RequestMapping("/v1")
@Api(value = "/v1")
@Validated
@RequiredArgsConstructor
public class FlightController {
    final FlightService flightService;

    @ApiOperation(value = "Read paginated subscription by filters", notes = "Read paginated flights  by filters")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", dataType = "integer", paramType = "query", value = "Results page you want to retrieve (0..N)"),
            @ApiImplicitParam(name = "size", dataType = "integer", paramType = "query", value = "Number of records per page."),
            @ApiImplicitParam(name = "sort", allowMultiple = true, dataType = "string", paramType = "query",
                    value = "Sorting criteria in the format: property(,asc|desc). " + "Default sort order is ascending. " + "Multiple sort criteria are supported.")})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success"),
            @ApiResponse(code = 400, message = "Request sent by the client was syntactically incorrect"),
            @ApiResponse(code = 404, message = "Resource access does not exist"),
            @ApiResponse(code = 500, message = "Internal server error during request processing")})
    @GetMapping(value = "/flights", produces = "application/json")
    @ResponseStatus(HttpStatus.OK)
    public Page<FlightDTO> readFlights(
            @ApiParam(value = "Airport Origin | Code IATA", required = false) @RequestParam(value = "airportOrigin", required = false) String airportOrigin,
            @ApiParam(value = "Airport Destination| Code IATA", required = false) @RequestParam(value = "airportDestination", required = false) String airportDestination,
            @ApiParam(value = "City Origin", required = false) @RequestParam(value = "cityOrigin", required = false) String cityOrigin,
            @ApiParam(value = "City Destination", required = false) @RequestParam(value = "cityDestination", required = false) String cityDestination,
            @ApiParam(value = "Departure date from", required = false) @RequestParam(value = "fromDepartureDate", required = false) @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss") Date fromDepartureDate,
            @ApiParam(value = "Departure date to", required = false) @RequestParam(value = "toDepartureDate", required = false) @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss") Date toDepartureDate,
            @ApiParam(value = "Arrival date from", required = false) @RequestParam(value = "fromArrivalDate", required = false) @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss") Date fromArrivalDate,
            @ApiParam(value = "Arrival date to", required = false) @RequestParam(value = "toArrivalDate", required = false) @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss") Date toArrivalDate,
            Pageable pageable) {

        /* Getting flights list */
        return flightService.readFlightByFilter(airportOrigin, airportDestination, cityOrigin, cityDestination, fromDepartureDate, toDepartureDate, fromArrivalDate, toArrivalDate, pageable);

    }
}
