package com.bizao.flightdata.service.entities.flight;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

@Data
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@Entity
@Table(name = "airport")
public class Airport {
    @Id
    @Column(name = "airport_iata")
    String airportIata;

    @Column(name = "airport_name")
    String airportName;

    @Column(name = "airport_city")
    String airportCity;

    @Column(name = "airport_country")
    String airportCountry;
}
