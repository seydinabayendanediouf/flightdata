package com.bizao.flightdata.service.entities.flight;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

@Data
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@Entity
@Table(name = "airline")
public class Airline {

    @Id
    @Column(name = "airline_iata", unique = true)
    String airlineIata;
    @Column(name = "airline_name", nullable = false)
    String airlineName;

    @Column(name = "airline_country")
    String airlineCountry;

}
