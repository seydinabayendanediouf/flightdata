package com.bizao.flightdata.service.entities;

import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import com.bizao.flightdata.service.entities.flight.Airline;
import com.bizao.flightdata.service.entities.flight.Airport;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

@Data
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@Entity
@Table(name = "flight")
public class Flight {
    @Id
    @Column(name = "flight_id", unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    Long flightId;

    @Column(name = "flight_price", nullable = false)
    BigDecimal price;

    @ManyToOne
    @JoinColumn(name = "airline",referencedColumnName="airline_iata", nullable = false)
    Airline airline;


    @ManyToOne
    @JoinColumn(name = "airport_origin",referencedColumnName="airport_iata", nullable = false)
    Airport airportOrigin;

    @ManyToOne
    @JoinColumn(name = "airport_destination", referencedColumnName="airport_iata", nullable = false)
    Airport airportDestination;

    @Column(name = "departure_date", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    Date departureDate;

    @Column(name = "arrival_date")
    @Temporal(TemporalType.TIMESTAMP)
    Date arrivalDate;
}
