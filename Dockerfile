FROM openjdk:8u111-jdk-alpine
VOLUME /tmp
ADD /target/flightdata-0.0.1-SNAPSHOT.jar app.jar
ENTRYPOINT ["java","-Dspring.profiles.active=prod", "-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar"]